# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>

log:
	mkdir -p $@

agp:
	ln -sf $(AGP) $@

scaff.gff:
	ln -sf $(SCAFF_GFF) $@

chr.gff:
	ln -sf $(CHR_GFF) $@

#### convert gff

conv.chr.gff: agp scaff.gff
	export PERL5LIB=$$PERL5LIB:$$PRJ_ROOT/local/src/; \
	gff3_agp_conversion \
	--agp $< \
	--gff2 $^2 \
	--out $@ \
	--verbose

conv.scaff.gff: agp chr.gff
	export PERL5LIB=$$PERL5LIB:$$PRJ_ROOT/local/src/; \
	gff3_agp_conversion \
	--agp $< \
	--gff2 $^2 \
	--out $@ \
	--reverse \
	--verbose

# check differences
.IGNORE: conv.chr.gff.Vs.chr
conv.chr.gff.Vs.chr: conv.chr.gff chr.gff
	sdiff -bBWs \
	<(bawk '!/^[\#+,$$]/ { print $$0; }' $<) \
	<(bawk '!/^[\#+,$$]/ { print $$0; }' $^2) \
	2>&1

conv.scaff.gff.Vs.scaff: conv.scaff.gff scaff.gff
	sdiff -bBWs \
	<(bawk '!/^[\#+,$$]/ { print $$0; }' $< | cut -f 1-8 ) \
	<(bawk '!/^[\#+,$$]/ { print $$0; }' $^2 | cut -f 1-8) \
	2>&1

#### convert bed

chr.bed:
	ln -sf $(CHR_BED) $@

# bed to gff3
chr.bed.gff3: chr.bed
	$(call load_modules); \
	gt bed_to_gff3 $< >$@

conv.scaff.bed.gff3: agp chr.bed.gff3
	export PERL5LIB=$$PERL5LIB:$$PRJ_ROOT/local/src/; \
	gff3_agp_conversion \
	--agp $< \
	--gff3 $^2 \
	--out $@ \
	--reverse

# gff3 to bed
conv.scaff.bed: conv.scaff.bed.gff3
	$(call load_modules); \
	gff2bed <$< >$@

.PHONY: test
test:
	@echo


ALL += conv.chr.gff \
	conv.scaff.gff

INTERMEDIATE +=

CLEAN +=