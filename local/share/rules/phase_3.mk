# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>


log:
	mkdir -p $@

agp:
	ln -sf $(AGP) $@

chr.fasta:
	ln -sf $(CHR_FASTA) $@

scaff.fasta:
	ln -sf $(SCAFF_FASTA) $@

chr.gff:
	ln -sf $(CHR_GFF) $@

scaff.gff:
	ln -sf $(SCAFF_GFF) $@

#### convert AGP to chain
#### AGP specifications: https://www.ncbi.nlm.nih.gov/assembly/agp/AGP_Specification/

chain: chr.fasta agp
	translate -a <(cat $< | fasta_length) 1 <$^2 \
	| bawk 'BEGIN { N=0; } \
	!/^[\#+,$$]/ { \
	if ( $$6 ~ "W" ) { \
		N++; \
		orientation=$$10; \
		if ( $$10 ~ "0" ) { orientation="+" } \
			print "chain 1000 "$$1" "$$2" + "$$3-1" "$$4" "$$7" "$$9" "orientation" "0" "$$9" "N; \
			print $$9; \
			print ""; } \
	} ' >$@

# reverse chain file
reverse.chain.tmp: chain
	$(call load_modules); \
	chainSwap $< $@

# this should join chain segments that are adjacent, but in practice it does not work
reverse.chain: reverse.chain.tmp
	$(call load_modules); \
	chainMergeSort $< >$@

#### convert gff

conv.scaff.gff: chain chr.gff
	$(call load_modules); \
	CrossMap.py gff $< $^2 >$@

conv.chr.gff: reverse.chain conv.scaff.gff
	$(call load_modules); \
	CrossMap.py gff $< <(cut -f 11- <$^2) >$@

conv.scaff.gff.Vs.scaff: conv.scaff.gff scaff.gff
	sdiff -bBWs \
	<(bawk '!/^[\#+,$$]/ { print $$0; }' $< | cut -f 11-16 ) \
	<(bawk '!/^[\#+,$$]/ { print $$0; }' $^2 | cut -f 1-6) \
	2>&1

conv.chr.gff.Vs.scaff: conv.chr.gff chr.gff
	sdiff -bBWs \
	<(bawk '!/^[\#+,$$]/ { print $$0; }' $< | cut -f 11-16 ) \
	<(bawk '!/^[\#+,$$]/ { print $$0; }' $^2 | cut -f 1-6) \
	2>&1

#### convert bed

chr.bed:
	ln -sf $(CHR_BED) $@

# generated with gff3_agp_conversion by Cristian
scaff.bed:
	ln -sf $(SCAFF_BED) $@

conv.scaff.bed: chain chr.bed
	$(call load_modules); \
	CrossMap.py bed $< $^2 >$@

# reverse the conversion.
# N.B.: segments that are splitted during conversion from chr -> scaffolds
# are not merged during the opposite conversion: scaffolds -> chr
# use mergeBed (bedtools) to merge this segments
conv.chr.bed: reverse.chain conv.scaff.bed
	$(call load_modules); \
	CrossMap.py bed $< <(cut -f 6-9 <$^2) >$@

# gff3_agp_conversion lost all regions that does not map
# on an whole contig or scaffold.
# CrossMap instead broke the mapping regions in different parts
conv.scaff.bed.Vs.scaff: conv.scaff.bed scaff.bed
	sdiff -bBWs \
	<( bawk '!/^[$$]/ { if ( $$5 !~ /split/ ) print $$6,$$7,$$8,$$9; }' $< \   * gff3_agp_conversion does not support splitted alignments *
		| bsort -k 2,2 ) \
	<(bawk '!/^[$$]/ { \
		split($$10,a,"="); \
		print $$1,$$2,$$3, a[2]; }' $^2 \
		| bsort -k 2,2 ) \
	2>&1

conv.chr.bed.Vs.chr: conv.chr.bed chr.bed
	$(call load_modules); \
	sdiff -bBWs \
	<( cut -f 6-9 $< \
		| sortBed -i stdin ) \
	<(sortBed -i $^2) \
	2>&1

#### convert bam

chr.bam:
	!threads
	$(call load_modules); \
	samtools sort -@ $$THREADNUM -m 4G -o - $(CHR_BAM) >$@

# generated with gff3_agp_conversion by Cristian
scaff.bam:
	!threads
	$(call load_modules); \
	samtools sort -@ $$THREADNUM -m 4G -o - $(SCAFF_BAM) >$@

# generate index
%.bam.bai: %.bam
	$(call load_modules); \
	samtools index $<

# translate
PRECIOUS: conv.scaff.bam
conv.scaff.bam: chain chr.bam.bai
	!threads
	$(call load_modules); \
	CrossMap.py bam -m 100000 -s 1000000 -t 100000 $< $(basename $^2) - \
	| samtools sort -@ $$THREADNUM -m 4G -o - - >$@

# translate reverse
PRECIOUS: conv.chr.bam
conv.chr.bam: reverse.chain scaff.bam.bai
	!threads
	$(call load_modules); \
	CrossMap.py bam -m 100000 -s 1000000 -t 100000 $< $(basename $^2) - \
	| samtools sort -@ $$THREADNUM -m 4G -o - - >$@


# check differences
# .IGNORE: scaff.bam.Vs.conv
# scaff.bam.Vs.conv: scaff.bam conv.scaff.bam
	# $(call load_modules); \
	# diff \
	# <(samtools view $< scaffold_0 \
	# | cut -f 1,3,4,5 \
	# | sort -n -k 1,1 ) \
	# <(samtools view $^2 scaffold_0 \
	# | cut -f 1,3,4,5 \
	# | sort -n -k 1,1 ) \
	# 2>&1

.IGNORE: scaff.bam.Vs.conv
scaff.bam.Vs.conv: scaff.bam.bai conv.scaff.bam.bai chr.bam.bai
	$(call load_modules); \
	samtools view $(basename $<) scaffold_0 \
	| cut -f 1-9 \
	| translate -d -j -a -v <(samtools view $(basename $^2) scaffold_0 \
	| cut -f 1-9) 1 \
	| translate -d -j -a -v <(samtools view $(basename $^3) chr17:1-13101952 \
	| cut -f 1-9) 1 \
	| head -n 1000 >$@

tot.scaffold_0: conv.scaff.bam.bai
	$(call load_modules); \
	samtools view $(basename $^3) scaffold_0 \
	| wc -l 


.PHONY: test
test:
	@echo 


ALL += 

INTERMEDIATE += reverse.chain.tmp

CLEAN += 
