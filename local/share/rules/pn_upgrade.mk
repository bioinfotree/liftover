# Copyright Michele Vidotto 2017 <michele.vidotto@gmail.com>


# target genome = old genome
target.agp:
	ln -sf $(TARGET_AGP) $@

# query genome = new genome
query.agp:
	ln -sf $(QUERY_AGP) $@

# repeats
target.te.chr.gff3:
	ln -sf $(TARGET_TE_CHR_GFF) $@
# genes
target.gene.chr.gff3:
	ln -sf $(TARGET_GENE_CHR_GFF) $@

query.chr.fasta:
	ln -sf $(QUERY_CHR_FASTA) $@

target.chr.fasta:
	ln -sf $(TARGET_CHR_FASTA) $@

scaff.fasta:
	ln -sf $(SCAFF_FASTA) $@

# microsatellities
target.ssr.chr.gff3:
	bawk '!/^[\#+,$$]/ { print $$1, ".", "item", $$2, $$3, ".", "+"; }' </projects/vitis/share/miculan/annotation/SSR-10bp_ReAS-TE_091130_consensi.txt >$@

# coordinates
target.nblrr.chr.gff3:
	bawk '!/^[\#+,$$]/ { \
	if ( $$2 < $$3 ) { print $$1, ".", "item", $$2, $$3, ".", "+"; } \
	else { print $$1, ".", "item", $$3, $$2, ".", "+"; } }' <NBLRRcoordinates12Xv0.txt >$@




target.chain: target.agp scaff.fasta target.chr.fasta
	$(call load_modules); \
	python -m jcvi.formats.chain fromagp $< $^2 $^3

query.chain: query.agp scaff.fasta query.chr.fasta
	$(call load_modules); \
	python -m jcvi.formats.chain fromagp $< $^2 $^3



# to correct the gff
# te.valid.gff3: chr.gff3
	# $(call load_modules); \
	# gt gff3 -addids yes -fixregionboundaries yes -sort yes -tidy yes -checkids yes $< >$@

##### use gff3_agp_conversion
# map the gff items from old chrs to scaffolds
scaff.%.chr.gff3: target.agp target.%.chr.gff3
	export PERL5LIB=$$PERL5LIB:$$PRJ_ROOT/local/src/; \
	gff3_agp_conversion \
	--agp $< \
	--gff3 $^2 \
	--out $@ \
	--reverse \
	--verbose

# map gff items from scaffolds to the new chrs
query.%.chr.gff3: query.agp scaff.%.chr.gff3
	export PERL5LIB=$$PERL5LIB:$$PRJ_ROOT/local/src/; \
	gff3_agp_conversion \
	--agp $< \
	--gff3 $^2 \
	--out $@ \
	--verbose


##### use crossmap
target.chain.swap: target.chain
	$(call load_modules); \
	chainSwap $< $@

# map the gff items from old chrs to scaffolds
cm.scaff.%.chr.gff3: target.chain.swap target.%.chr.gff3
	$(call load_modules); \
	CrossMap.py gff $< $^2 >$@

# map gff items from scaffolds to the new chrs
cm.query.%.chr.gff3: query.chain cm.scaff.%.chr.gff3
	$(call load_modules); \
	CrossMap.py gff $< $^2 $@


##### convet SNPs
TARGET_SNP_BASENAME = $(notdir $(TARGET_SNP))
TARGET_SNP_LINK = $(addprefix target.,$(TARGET_SNP_BASENAME:all_var_vinifera_v2.%=%))

target.chr%.goodReg.txt:
	@echo "Installing links..."
	@echo $(foreach TAB, $(TARGET_SNP), \
		$(shell \
			FILE=$$(basename $(TAB)); \
			FILENAME="$${FILE#all_var_vinifera_v2.}"; \
			ln -sf $(TAB) target."$$FILENAME"; \
		)\
	)
	sleep 10

# From Variant Call Format v4 (VCF) to extended BED data: 
# convert from 1-based, closed [start, end] to 0-based, half-open [start-1, end)
cm.scaff.chr%.goodReg.txt: target.chain.swap target.chr%.goodReg.txt
	$(call load_modules); \
	CrossMap.py bed $< <(awk 'BEGIN { FS="\t" } /^chr/ { \
	printf "%s\t%s\t%s\t", $$1, $$2-1, $$2; \
	for ( i=3; i<NF; i++ ) { \
		printf "%s:", $$i; } \
	printf "%s\n", $$NF; }' $^2) $@

QUERY_SNP = $(addprefix query.,$(TARGET_SNP_LINK:target.%=%))

query.chr%.goodReg.txt: query.chain cm.scaff.chr%.goodReg.txt target.chr%.goodReg.txt
	$(call load_modules); \
	CrossMap.py bed $< $^2 \
	| select_columns 6 8 9 \
	| tr ":" \\t \
	| cat <(head -n 1 $^3) - >$@   * replace header *


##### conversion 1
TARGET_SNP_BASENAME1 = $(shell echo "$(notdir $(TARGET_SNP1))" | sed 's/raw.//g')
TMP_TARGET_SNP_LINK1 = $(addprefix target1.,$(TARGET_SNP_BASENAME1:all_var_vinifera_v2.%=%))
TARGET_SNP_LINK1 = $(TMP_TARGET_SNP_LINK1:.txt=.goodReg.txt)

target1.chr%.goodReg.txt:
	@echo "Installing links..."
	@echo $(foreach TAB, $(TARGET_SNP1), \
		$(shell \
			FILE=$$(echo "$$(basename $(TAB))" | sed 's/txt/goodReg.txt/g'); \
			FILENAME="$${FILE#raw.all_var_vinifera_v2.}"; \
			ln -sf $(TAB) target1."$$FILENAME"; \
		)\
	)
	sleep 10

# From Variant Call Format v4 (VCF) to extended BED data: 
# convert from 1-based, closed [start, end] to 0-based, half-open [start-1, end)
cm.scaff1.chr%.goodReg.txt: target.chain.swap target1.chr%.goodReg.txt
	$(call load_modules); \
	CrossMap.py bed $< <(awk 'BEGIN { FS="\t" } /^chr/ { \
	printf "%s\t%s\t%s\t", $$1, $$2-1, $$2; \
	for ( i=3; i<NF; i++ ) { \
		printf "%s:", $$i; } \
	printf "%s\n", $$NF; }' $^2) $@

QUERY_SNP1 = $(addprefix query1.,$(TARGET_SNP_LINK1:target1.%=%))

query1.chr%.goodReg.txt: query.chain cm.scaff1.chr%.goodReg.txt target1.chr%.goodReg.txt
	$(call load_modules); \
	CrossMap.py bed $< $^2 \
	| select_columns 6 8 9 \
	| tr ":" \\t \
	| cat <(head -n 1 $^3) - >$@   * replace header *



##### conversion 2
TARGET_SNP_BASENAME2 = $(shell echo "$(notdir $(TARGET_SNP2))" | sed 's/.PN40024.txt/.txt/g')
TARGET_SNP_LINK2 = $(addprefix target2.,$(TARGET_SNP_BASENAME2:all_var_vinifera_v2.%=%))

target2.chr%.goodReg.txt:
	@echo "Installing links..."
	@echo $(foreach TAB, $(TARGET_SNP2), \
		$(shell \
			FILE=$$(echo "$$(basename $(TAB))" | sed 's/PN40024.//g'); \
			FILENAME="$${FILE#all_var_vinifera_v2.}"; \
			ln -sf $(TAB) target2."$$FILENAME"; \
		)\
	)
	sleep 10

# From Variant Call Format v4 (VCF) to extended BED data: 
# convert from 1-based, closed [start, end] to 0-based, half-open [start-1, end)
cm.scaff2.chr%.goodReg.txt: target.chain.swap target2.chr%.goodReg.txt
	$(call load_modules); \
	CrossMap.py bed $< <(awk 'BEGIN { FS="\t" } /^chr/ { \
	printf "%s\t%s\t%s\t", $$1, $$2-1, $$2; \
	for ( i=3; i<NF; i++ ) { \
		printf "%s:", $$i; } \
	printf "%s\n", $$NF; }' $^2) $@

QUERY_SNP2 = $(addprefix query2.,$(TARGET_SNP_LINK2:target2.%=%))

query2.chr%.goodReg.txt: query.chain cm.scaff2.chr%.goodReg.txt target2.chr%.goodReg.txt
	$(call load_modules); \
	CrossMap.py bed $< $^2 \
	| select_columns 6 8 9 \
	| tr ":" \\t \
	| cat <(head -n 1 $^3) - >$@   * replace header *

##### conversion 3

# From Variant Call Format v4 (VCF) to extended BED data: 
# convert from 1-based, closed [start, end] to 0-based, half-open [start-1, end)
cm.scaff3.txt: target.chain.swap
	$(call load_modules); \
	CrossMap.py bed $< <(awk 'BEGIN { FS="\t" } /^chr/ { \
	printf "%s\t%s\t%s\t", $$1, $$2-1, $$2; \
	for ( i=3; i<NF; i++ ) { \
		printf "%s:", $$i; } \
	printf "%s\n", $$NF; }' $(TARGET_SNP3)) $@

query3.txt: query.chain cm.scaff3.txt
	$(call load_modules); \
	CrossMap.py bed $< $^2 \
	| select_columns 6 8 9 \
	| tr ":" \\t \
	| cat <(head -n 1 $(TARGET_SNP3)) - >$@   * replace header *

##### conversion 4
TARGET_SNP_BASENAME4 = $(shell echo "$(notdir $(TARGET_SNP4))")
TARGET_SNP_LINK4 = $(addprefix target4.,$(TARGET_SNP_BASENAME4:batch_1.%=%))

target4.%.loc:
	@echo "Installing links..."
	@echo $(foreach TAB, $(TARGET_SNP4), \
		$(shell \
			FILE=$$(echo "$$(basename $(TAB))"); \
			FILENAME="$${FILE#batch_1.}"; \
			ln -sf $(TAB) target4."$$FILENAME"; \
		)\
	)
	sleep 10

# From Variant Call Format v4 (VCF) to extended BED data: 
# convert from 1-based, closed [start, end] to 0-based, half-open [start-1, end)
cm.scaff4.rephased_clean_curated_nohomo_noReAS_64_genomic_sorted_ALL.loc: target.chain.swap /projects/novabreed/share/afornasiero/RAD_seq/project_data/schiava_grossa/91samples_300-450/cleaned_files/rephased_clean_curated_nohomo_noReAS_64_genomic_sorted_ALL.loc
	$(call load_modules); \
	CrossMap.py bed $< <(awk 'BEGIN { FS="\t" } /^chr/ { \
	printf "%s\t%s\t%s\t", $$1, $$2-1, $$2; \
	for ( i=3; i<NF; i++ ) { \
		printf "%s:", $$i; } \
	printf "%s\n", $$NF; }' $^2) $@

QUERY_SNP4 = $(addprefix query4.,$(TARGET_SNP_LINK4:target4.%=%))

query4.rephased_clean_curated_nohomo_noReAS_64_genomic_sorted_ALL.loc: query.chain cm.scaff4.rephased_clean_curated_nohomo_noReAS_64_genomic_sorted_ALL.loc
	$(call load_modules); \
	CrossMap.py bed $< $^2 \
	| select_columns 6 8 9 \
	| tr ":" \\t >$@

.PHONY: crossmap
crossmap: target.chain.swap /projects/novabreed/share/afornasiero/RAD_seq/project_data/schiava_grossa/91samples_300-450/cleaned_files/rephased_clean_curated_nohomo_noReAS_64_genomic_sorted_ALL.loc
	$(call load_modules); \
	cat $^2 \
	| crossmap -c $< 1


.PHONY: test
test:
	@echo $(TARGET_SNP_BASENAME4)


ALL += query.te.chr.gff3 \
	query.gene.chr.gff3 \
	cm.query.te.chr.gff3 \
	cm.query.gene.chr.gff3 \
	$(QUERY_SNP1) \
	$(QUERY_SNP2) \
	$(QUERY_SNP)

INTERMEDIATE +=

CLEAN +=