# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>

context prj/check_hemizygosity


log:
	mkdir -p $@

tmp:
	mkdir -p $@

scaff.bam:
	ln -sf $(SCAFF_BAM) $@

contig.bam:
	ln -sf $(CONTIG_BAM) $@

test.final.summary:
	ln -sf $(FINAL_SUMMARY) $@

test.scaffolds.fasta:
	ln -sf $(SCAFFOLDS) $@

# bed file for only one scaffold
scaffold_0.contigs.bed: test.final.summary
	$(call load_modules); \
	scaffolds2contigs <$< \
	| cut -f 1-5 \
	| grep -E 'contig_[0-9]+' \
	| grep -E 'scaffold_0' \
	| sortBed -i stdin >$@

# for testing purpose
chain.new: test.final.summary
	$(call load_modules); \
	scaffolds2contigs -c <$< >$@


contigs.bed: test.final.summary
	$(call load_modules); \
	scaffolds2contigs <$< \
	| cut -f 1-5 \
	| grep -E 'contig_[0-9]+' \
	| sortBed -i stdin >$@



# chain	10	scaffold_9999	939	+	1	939	contig_113919	939	+	0	939	114026
# 939
chain: test.scaffolds.fasta contigs.bed
	translate -a <(cat $< | fasta_length) 1 <$^2 \
	| bawk 'BEGIN { N=0; } \
	!/^[\#+,$$]/ { \
	N++; \
		print "chain 1000 "$$1" "$$2" + "$$3" "$$4" "$$5" "$$6" + "0" "$$6" "N; \
		print $$6; \
		print ""; \
	} ' >$@

# reverse chain file
reverse.chain.tmp: chain
	$(call load_modules); \
	chainSwap $< $@

# this should join chain segments that are adjacent, but in practice it does not work
reverse.chain: reverse.chain.tmp
	$(call load_modules); \
	chainMergeSort $< >$@

############ Generate chain file with from scratch using BLAT ############

contigs.fasta: scaffold_0.contigs.bed
	fasta_get -f <(cut -f4 $<) <$(CONTIGS) >$@

scaffolds.fasta:
	fasta_get scaffold_0 <$(SCAFFOLDS) >$@

# save list of chunks to make var
chunks.mk:
	ARRAY=( $$(seq -s " " $(SPLIT)) ); \
	echo "FASTA_CHUNCKS := $${ARRAY[@]/#/contigs.fasta.}" >$@

include chunks.mk

# split fasta
contigs.fasta.%: contigs.fasta
	$(call load_modules); \
	if [ "$*" == "1" ]; then \ 
		gt splitfasta -numfiles $(SPLIT) -force yes $<; \
	else \
		sleep 5; \   * wait untill all pieces are generated *
	fi

LFT = $(addprefix contigs.lft.,$(shell seq $(SPLIT)))
contigs.lft.%: contigs.fasta.%
	$(call load_modules); \
	fasta2tab <$< \
	| bawk '!/^[\#+,$$]/ { print 0, $$1, length($$2), $$1, length($$2); }' >$@


scaffolds.size: scaffolds.fasta
	fasta_length <$< >$@

contigs.size: contigs.fasta
	fasta_length <$< >$@


reference.2bit: scaffolds.fasta
	$(call load_modules); \
	faToTwoBit $< $@

contigs.2bit: contigs.fasta
	$(call load_modules); \
	faToTwoBit $< $@

new.reference.2bit: reference.2bit scaffolds.size
	$(call load_modules); \
	twoBitInfo $< $^2

new.contigs.2bit: contigs.2bit contigs.size
	$(call load_modules); \
	twoBitInfo $< $^2

BLAT_PSL = $(addsuffix .psl,$(addprefix contigs.,$(shell seq $(SPLIT))))
contigs.%.psl: log reference.2bit contigs.fasta.%
	$(call load_modules); \
	/usr/bin/time -v \
	blat \
	-tileSize=11 \
	-minScore=100 \
	-minIdentity=98 \
	-noHead \
	$^2 $^3 $@ \
	2>&1 \
	| tee $</$@.mm.blat

LIFTUP_PSL = $(addsuffix .psl,$(addprefix liftup.contigs.,$(shell seq $(SPLIT))))
liftup.contigs.%.psl: contigs.lft.% contigs.%.psl
	$(call load_modules); \
	liftUp -pslQ $@ $< warn $^2

CHAIN = $(addsuffix .chain,$(addprefix contigs.,$(shell seq $(SPLIT))))
contigs.%.chain: liftup.contigs.%.psl reference.2bit contigs.2bit
	$(call load_modules); \
	axtChain \
	-linearGap=medium \
	-psl $< \
	$^2 \
	$^3 \
	$@

generated.chain: $(CHAIN)
	$(call load_modules); \
	chainMergeSort $^ >$@

############ Generate chain file with from scratch using BLAT ############

# generate index
%.bam.bai: %.bam
	$(call load_modules); \
	samtools index $<

PRECIOUS: conv.scaff.bam
conv.contig.bam: chain scaff.bam.bai
	!threads
	$(call load_modules); \
	CrossMap.py bam -m 100000 -s 1000000 -t 100000 $< $(basename $^2) - \
	| samtools sort -@ $$THREADNUM -m 4G -o - - >$@

# check differences
# .IGNORE: ori.Vs.new
# ori.Vs.new: contig.bam contigs.new.bam scaffold_0.contigs.bed
	# $(call load_modules); \
	# sdiff -bBWs \
	# <(samtools view -L <(bawk '!/^[\#+,$$]/ { print $$4, 0, $$5; }' $^3) $< \
	# | cut -f 1,2,3,4,5,6,7,8,9,10,11 \
	# | sort -n -k 1,1 ) \
	# <(samtools view -L <(bawk '!/^[\#+,$$]/ { print $$4, 0, $$5; }' $^3) $^2 \
	# | cut -f 1,2,3,4,5,6,7,8,9,10,11 \
	# | sort -n -k 1,1 ) \
	# 2>&1 >$@

.IGNORE: contig.bam.Vs.conv
contig.bam.Vs.conv: contig.bam.bai conv.contig.bam.bai scaff.bam.bai
	$(call load_modules); \
	samtools view $(basename $<) contig_0 \
	| cut -f 1-9 \
	| translate -d -j -a -v <(samtools view $(basename $^2) contig_0 \
	| cut -f 1-9) 1 \
	| translate -d -j -a -v <(samtools view $(basename $^3) scaffold_0:0-6591 \
	| cut -f 1-9) 1 \
	| head -n 1000 >$@

tot.scaffold_0: contig.bam contigs.new.bam scaffold_0.contigs.bed
	$(call load_modules); \
	samtools view -L <(bawk '!/^[\#+,$$]/ { print $$4, 0, $$5; }' $^3) $< \
	| wc -l 



# reverse conversion
PRECIOUS: conv.scaff.bam
conv.scaff.bam: reverse.chain contig.bam.bai
	!threads
	$(call load_modules); \
	CrossMap.py bam -m 350 -s 90 -t 3 $< $(basename $^2) - \
	| samtools sort -@ $$THREADNUM -m 4G -o - - >$@

conv.scaff.fixmate.bam: log tmp conv.scaff.bam
	!threads
	$(call load_modules); \
	java -Xmx10g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar FixMateInformation \
	INPUT=$^3 \
	OUTPUT=$@ \
	TMP_DIR=$^2 \
	SORT_ORDER=coordinate \
	VALIDATION_STRINGENCY=LENIENT \
	3>&1 1>&2 2>&3 \
	| tee $</picard-FixMateInformation.$@.log


.IGNORE: scaff.bam.Vs.conv
scaff.bam.Vs.conv: scaff.bam.bai conv.scaff.fixmate.bam.bai contig.bam.bai
	$(call load_modules); \
	samtools view $(basename $<) scaffold_0:0-6591 \
	| cut -f 1-9 \
	| translate -d -j -a -v <(samtools view $(basename $^2) scaffold_0:0-6591 \
	| cut -f 1-9) 1 \
	| translate -d -j -a -v <(samtools view $(basename $^3) contig_0 \
	| cut -f 1-9) 1 \
	| head -n 1000 >$@






.PHONY: test
test:
	@echo $(LIFTUP_PSL)


ALL += generated.chain

INTERMEDIATE +=

CLEAN += chain \
	contigs.new.bam \
	contigs.new.bam.bai \
	ori.Vs.new