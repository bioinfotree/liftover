# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>


log:
	mkdir -p $@

agp:
	ln -sf $(AGP) $@

chr.fasta:
	ln -sf $(CHR_FASTA) $@

chain: agp chr.fasta
	$(call load_modules); \
	fasta_length <$^2 \
	| agp2chain -f $< >$@

$(THIS_PATH)/%.ordering.trans: agp $(THIS_PATH)/%.ordering
	bawk '!/[\#+,$$]/ { print $$0 }' <$^2 \
	| translate -a <(select_columns 6 1 2 3 <$< | grep -E '^scaffold') 2 >$@

.PHONY: convert.bed
convert.bed: chain
	$(call load_modules); \
	CrossMap.py bed $< $(BED) >$(dir $(BED))/converted.$(notdir $(BED))



.PHONY: test
test:
	@echo 


ALL += agp \
	$(FILE:.ordering=.ordering.trans)

INTERMEDIATE += 

CLEAN += 
