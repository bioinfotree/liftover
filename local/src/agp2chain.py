#!/usr/bin/env python

# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

## TODO:

from argparse import ArgumentParser, RawDescriptionHelpFormatter
from vfork.util import exit, format_usage, ignore_broken_pipe
from vfork.io.util import safe_rstrip
from jcvi.formats.agp import AGP
from sys import stdin, stdout

def parseArgs():
	parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter, 
	description=format_usage('''
Convert a GenBank A Golden Path (AGP) file into an UCSC CHAIN file and reverse.

For AGP to CHAIN conversion, a tab-delimited FILE must be given to STDIN. Each line must contain 
the object name in the first column (ie: chr) and the length of the object in the second column, with one
line for each distinct reference. This file can be generated with samtools faidx <ref.fa>.

Gaps are not reported in the resulting AGP file. Results are returned to STDOUT.
	''' ))
	parser.add_argument('-f','--file', metavar='FILE', dest='conv_file', required=True, help='file to be converted. Can be in AGP format or CHAIN format. Default is AGP format.')
	parser.add_argument('-r','--reverse', action='store_true', dest='reverse', help='reverse the conversion: CHAIN to AGP.')
	args = parser.parse_args()
	return args

def main():
	args = parseArgs()

	length = {}
	for line in (stdin):
		if line.startswith('#'):
			continue
		token = safe_rstrip(line).split()
		length[token[0]] = int(token[1])

	i = 1

	if args.reverse:

		with open(args.conv_file, 'r') as rfh:
			for j, line in enumerate(rfh):
				if line.startswith('chain'):

					strand = '+'
					token = safe_rstrip(line).split()

					object = token[2]
					object_beg = int(token[5]) + 1
					object_end = int(token[6])

					component_id = token[7]
					component_beg = int(token[10]) +1
					component_end = int(token[11])
					strand = token[9]

					if token[4] == '-' and token[9] == '+': # if the object map in negative strand coordinates are converted
						object_beg = length[object] - int(token[6]) +1
						object_end = length[object] - int(token[5])

					if token[4] == '-' and token[9] == '-':
						exit('reverse strand for both object %s and component %s at line %i' %(object, component_id, j+1))

					#chr10	1	1180538	1	W	scaffold_116	1	1180538	+
					stdout.write('\t'.join(str(x) for x in (object, object_beg, object_end, i, 'W', component_id, component_beg, component_end, strand, '\n')))
					i+=1

		return 0


	agp = AGP(args.conv_file)

	for a in agp:
		if a.is_gap:
			continue
		strand = '-' if a.orientation == '-' else '+'
		stdout.write(' '.join(str(x) for x in (a.object, length[a.object], a.object_beg-1, a.object_end, a.component_id, a.component_end, strand, a.component_beg-1, a.component_end, i, '\n')))
		stdout.write('%i\n\n' %a.component_end)
		i+=1

	return 0

if __name__ == '__main__':
	ignore_broken_pipe(main)